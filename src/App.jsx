import Login from "./components/Login";
import {createBrowserRouter, Outlet,RouterProvider} from "react-router-dom"
import Layout from "./components/layout";
import Home from "./components/Home";
import Register from "./components/Register";


const router =createBrowserRouter([{
  path:"/",
  element:<Layout/>,
  children:[{
    path:'/',
    element:<Home/>
  },
    {
    path:'/login',
    element:<Login/>
  },
  {
    path:'/register',
    element:<Register/>
  }]
}])
function App() {

  return (
    <div className="App">
    <RouterProvider router={router}/> 
    </div>
  );
}

export default App;
