import React, { useState } from 'react'
import vector from "./images/Vector.png"
import vector1 from "./images/vector1.png"
import user from "./images/user.png"
import lock from "./images/lock.png"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons'
import axios from 'axios'
import { Link } from 'react-router-dom'

const Login = () => {
  const [required,setRequired]=useState({
    username:false,
    password:false,
  })
  const [visible,setVisible]=useState(false)
  const[forms,setForms]=useState({
    username:"",
    password:""
  })
  const handlechange=(e)=>{
    setRequired({...required,[e.target.name]:false})
    setForms({...forms,[e.target.name]:e.target.value})
  }

  const handleSubmit=async(e)=>{
    e.preventDefault()
    if(!forms.username){
      setRequired(prev=>({...prev,username:true}))
    }
      if(!forms.password){
      setRequired(prev=>({...prev,password:true}))
    }
    try{
      const res=await axios.post("/login",forms)
    }
    catch(err){
      console.log(err)
    }

  }

  return (
    <div className='flex justify-center items-center h-screen bg-[#232237] '>
      <div >
        <img src={vector} alt="" className='absolute right-0 top-0 h-screen '/>
      </div>
      <div className='h-[45%] w-[20%]  z-10'>
        <form action="" onSubmit={handleSubmit} className='flex flex-col justify-center h-full space-y-5'>
          <div className='space-y-2 '>
            <div className='flex w-full relative'>
              <img src={user} alt=""  className='absolute top-[15%] left-[2%]  '/>
              <input type="text" placeholder='username' name='username' onChange={handlechange}className=' : w-full text-white text-lg pl-8 pr-2 py-1 bg-[#232237]  border border-white placeholder:text-white placeholder:font-light placeholder:uppercase placeholder:text-sm rounded-md  outline-none' />
            </div>
            {required.username &&  <p className='text-red-500 pl-5 text-sm font-medium h-3'>Username is required !</p> }
          </div>
          <div className='space-y-2'>
            <div className='flex w-full relative'>
              <img src={lock} alt=""  className='absolute top-[20%] left-[2%] '/>
              <input type={visible ? "text":"password"} placeholder='password' name='password' onChange={handlechange} className='w-full text-white text-lg pl-8 pr-8 py-1 font-light bg-[#232237]  border border-white placeholder:text-white placeholder:uppercase placeholder:text-sm rounded-md outline-none'/>
              <FontAwesomeIcon icon={visible? faEyeSlash:faEye} onClick={()=>setVisible(!visible)} className='absolute top-[30%] right-[3%] text-white cursor-pointer'/>
            </div>
            {required.password &&  <p className='text-red-500 pl-5 text-sm font-medium h-3'>Password is required !</p> }
          </div>
         
          <button className='uppercase w-full bg-[#9A40F4] text-white drop-shadow-2xl rounded-md py-2 '>login</button>
          <div className='flex justify-center space-x-2 text-xs w-full'>
              <p className='text-white font-normal'>Don't have an account? </p>
              <span className='text-bgreen font-semibold drop-shadow-sm  text-[#9A40F4]'><Link to="/register">Sign up for free !</Link></span>
          </div>
        </form>
      </div>
      <div >
        <img src={vector1} alt="" className='absolute left-0 bottom-0 h-[50%] '/>
      </div>
      
    </div>
  )
}

export default Login